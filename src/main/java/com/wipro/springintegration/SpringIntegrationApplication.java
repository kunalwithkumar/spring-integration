package com.wipro.springintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;

@SpringBootApplication
public class SpringIntegrationApplication {

	public static void main(final String[] args) {
		SpringApplication.run(SpringIntegrationApplication.class, args);
	}

	@Bean
	public MessageChannel recieverMessageChannel() {
		return new DirectChannel();
	}

	@Bean
	public MessageChannel replyMessageChannel() {
		return new DirectChannel();
		// acts like a point-to -point
	}

//	@Autowired
//	private TweetPublisher tweetPublisher;
//	@Autowired
//	private Tweeter tweeter;
//	@Autowired
//	private DirectChannel channel;
//
//	@Bean
//	public MessageChannel tweetChannel() {
//		this.channel = new DirectChannel();
//		return channel;
//	}
//
//	@Bean
//	public CommandLineRunner commandLineRunner(final ApplicationContext context) {
//		return args -> {
//			channel.subscribe(new TweetReader());
//			channel.subscribe(new TweetReader());
//			channel.subscribe(new TweetReader());
//			final List<Tweet> tweets = tweetPublisher.getTweets();
//			for (final Tweet tweet : tweets) {
//				tweeter.sendTweetReaders(tweet);
//			}
//		};
//	}

}
