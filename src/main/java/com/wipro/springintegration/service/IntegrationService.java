package com.wipro.springintegration.service;

import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

@Component
public class IntegrationService {

	@ServiceActivator(inputChannel = "integration.gateway.channel")
	public void message(final Message<String> message) throws MessagingException {

		final MessageChannel replyChannel = (MessageChannel) message.getHeaders().getReplyChannel();

		MessageBuilder.fromMessage(message);

		final Message<String> newMessage = MessageBuilder
				.withPayload("Hellooo " + message.getPayload() + " to integration").build();

		replyChannel.send(newMessage);

	}

}
