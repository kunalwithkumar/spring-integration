package com.wipro.springintegration.model;

import java.io.Serializable;

public class TransformTest implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String data;
	private Double value;

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(final String data) {
		this.data = data;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(final Double value) {
		this.value = value;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "TransformTest [id=" + id + ", data=" + data + ", value=" + value + "]";
	}

	public TransformTest() {
		// TODO Auto-generated constructor stub
	}

	public TransformTest(final Long id, final String data, final Double value) {
		this.id = id;
		this.data = data;
		this.value = value;
	}

}
